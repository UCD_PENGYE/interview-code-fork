import unittest
import sys
import mycode


class TestMycode(unittest.TestCase):
    def setUp(self):
        self.__debug = False
        self.__my = mycode.generator()

    def tearDown(self):
        pass

    def test_001(self):
        '''
        Verify generate_password with length and Complexity 1
        accosciating with check_password_level method
        '''
        for i in range(1, 8):
            password = self.__my.generate_password(i, 1)
            print("testing...with {}".format(password))
            complexity = self.__my.check_password_level(password)
            self.assertTrue(complexity == 1,
                            "FAIL: {} is not complexity-1 password: "
                            "complexity {}".format(password, complexity))
        print("PASS: checking generate_password() with complexity 1")

    def test_002(self):
        '''
        Verify generate_password with length and Complexity 2, associating
        with check_password_level method
        '''
        for i in range(2, 8):
            password = self.__my.generate_password(i, 2)
            print("testing...with {}".format(password))
            complexity = self.__my.check_password_level(password)
            self.assertTrue(complexity == 2,
                            "FAIL: {} is not complexity-2 password: "
                            "complexity {}".format(password, complexity))
        # for those password length >=8 and obey rule of complexity 1
        # will be re-marked as complexity 2
        for i in range(8, 16):
            password = self.__my.generate_password(i, 1)
            print("testing...with {}".format(password))
            complexity = self.__my.check_password_level(password)
            self.assertTrue(complexity == 2,
                            "FAIL: {} is not complexity-2 password: "
                            "complexity {}".format(password, complexity))
        print("PASS: checking generate_password() with complexity 2")

    def test_003(self):
        '''
        Verify generate_password with length and Complexity 3,
        associating with check_password_level method
        '''
        for i in range(3, 8):
            password = self.__my.generate_password(i, 3)
            print("testing...with {}".format(password))
            complexity = self.__my.check_password_level(password)
            self.assertTrue(complexity == 3,
                            "FAIL: {} is not complexity-3 password: "
                            "complexity {}".format(password, complexity))
        # for those password length >=8 and obey rule of complexity-2
        # will be re-marked as complexity-3
        for i in range(8, 16):
            password = self.__my.generate_password(i, 2)
            print("testing...with {}".format(password))
            complexity = self.__my.check_password_level(password)
            self.assertTrue(complexity == 3,
                            "FAIL: {} is not complexity-3 password: "
                            "complexity {}".format(password, complexity))
        print("PASS: checking generate_password() with complexity 3")

    def test_004(self):
        '''
        Verify generate_password with length and Complexity 4, associating
        with check_password_level method
        '''
        for i in range(4, 16):
            password = self.__my.generate_password(i, 4)
            print("testing...with {}".format(password))
            complexity = self.__my.check_password_level(password)
            self.assertTrue(complexity == 4,
                            "FAIL: {} is not complexity-4 password: "
                            "complexity {}".format(password, complexity))
        print("PASS: checking generate_password() with complexity-4")

    def test_005(self):
        '''
        Boundary teting when length is 1,2,3 and less 1
        '''
        for length in range(4):
            C = length+1
            password = self.__my.generate_password(length, C)
            self.assertTrue(len(password) == 0,
                            "FAIL: length of {} can not generate "
                            "complexity-{} password".format(length, C))
        print("PASS: Boundary checking generate_password() "
              "with special length")

if __name__ == '__main__':
    # Select whole test cases into test plate
    suite = unittest.TestLoader().loadTestsFromTestCase(TestMycode)

    # Select particular single test case into test plate
    # suite = unittest.TestSuite()
    # suite.addTest(TestAnn("test_001"))

    # Start to run test cases
    unittest.TextTestRunner(verbosity=2).run(suite)
