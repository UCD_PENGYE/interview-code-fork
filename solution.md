1.mycode.py
I used class names generator, in which there are 3 major methods:
    generate_password()
    check_password_level()
    create_user()

In each method, there are serveral nested function definiations

2. test.py
This script is developed based on unittest lib. There are 5 unit test cases in total, by which I can easily check if my module mycode.py works well.

3. dispatch.py
This is for the last question, which could generate 10 users' password and persist their information (name and email) including password into a given
SQLite db file.

4. My interview video URL from google drive:
https://drive.google.com/file/d/1Om-r3gmjs7fyFhFkI6PAnB5l4Ra4mW-z/view?usp=sharing

5. My interview video URL from Youtube:
https://youtu.be/dABcz5xlP0g
