import sqlite3
import mycode
import os
import random


class dispatch:
    def __init__(self, number):
        self.__mycoderun = None
        self.__number = number
        self.__db = 'example.db'
        self.__mycoderun = mycode.generator()

    def _get_name(self, password):
        path = os.path.dirname(os.path.abspath(__file__))
        db_path = '/'.join([path, self.__db])
        conn = sqlite3.connect(db_path)
        c = conn.cursor()
        c.execute("select Name from persons order by persons.timeStamp desc")
        name = c.fetchone()[0]
        c.execute('''UPDATE persons SET Password = ? WHERE Name = ?''',
                  (password, name))
        conn.commit()

        c.execute("select * from persons order by persons.timeStamp desc")
        print(c.fetchone())
        conn.close()

    def create_password(self):
        length = random.randint(6, 12)
        complexity = random.randint(1, 4)
        return self.__mycoderun.generate_password(length, complexity)

    def run(self):
        for i in range(self.__number):
            self.__mycoderun.create_user(self.__db)
            self._get_name(self.create_password())


if __name__ == '__main__':
    my = dispatch(10)
    my.run()
