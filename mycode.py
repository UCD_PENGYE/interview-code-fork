import requests
import random
import sqlite3
import string
import re
import os
import time


class generator:
    def __init__(self):
        pass

    def generate_password(self, length: int, complexity: int) -> str:
        """Generate a random password with given length and complexity

        Complexity levels:
            Complexity == 1: return a password with only lowercase chars
            Complexity ==  2: Previous level plus at least 1 digit
            Complexity ==  3: Previous levels plus at least 1 uppercase char
            Complexity ==  4: Previous levels plus at least 1 punctuation char

        :param length: number of characters
        :param complexity: complexity level
        :returns: generated password
        """
        lower_str = string.ascii_lowercase
        upper_str = string.ascii_uppercase
        digit_str = string.digits
        pun_str = string.punctuation

        source = {'pass': '',
                  'lower': lower_str,
                  'upper': upper_str,
                  'digit': digit_str,
                  'punc': pun_str}

        def level_1(source):
            if length < 1:
                return ''
            for i in range(length):
                num = random.randint(0, len(source['lower'])-1)
                source['pass'] += source['lower'][num]
            return source['pass']

        def level_2(source):
            if length < 2:
                return ''
            source_str = source['lower'] + source['digit']
            for i in range(length):
                num = random.randint(0, len(source_str)-1)
                source['pass'] += source_str[num]
            if len([c for c in source['pass'] if c.isdigit()]) == 0:
                num = random.randint(0, len(source['pass'])-1)
                digit = source['digit'][random.randint(0, 9)]
                source['pass'] = \
                    source['pass'].replace(source['pass'][num], digit, 1)
            if len([c for c in source['pass'] if c.islower()]) == 0:
                num = random.randint(0, len(source['pass'])-1)
                char = source['lower'][random.randint(0, 25)]
                source['pass'] = \
                    source['pass'].replace(source['pass'][num], char, 1)
            return source['pass']

        def level_3(source):
            if length < 3:
                return ''
            source_str = \
                source['lower'] + source['digit'] + source['upper']
            for i in range(length):
                num = random.randint(0, len(source_str)-1)
                source['pass'] += source_str[num]
            is_ok = False
            while not is_ok:
                is_ok = True
                if len([c for c in source['pass'] if c.isdigit()]) == 0:
                    is_ok = False
                    digit = source['digit'][random.randint(0, 9)]
                    source['pass'] = \
                        source['pass'].replace(source['pass'][0], digit, 1)
                if len([c for c in source['pass'] if c.islower()]) == 0:
                    is_ok = False
                    char = source['lower'][random.randint(0, 25)]
                    source['pass'] = \
                        source['pass'].replace(source['pass'][1], char, 1)
                if len([c for c in source['pass'] if c.isupper()]) == 0:
                    is_ok = False
                    char = source['upper'][random.randint(0, 25)]
                    source['pass'] = \
                        source['pass'].replace(source['pass'][2], char, 1)
            return source['pass']

        def ispunctuation(char):
            return char in string.punctuation

        def level_4(source):
            if length < 4:
                return ''
            source_str = \
                source['lower'] + \
                source['digit'] + \
                source['upper'] + \
                source['punc']
            for i in range(length):
                num = random.randint(0, len(source_str)-1)
                source['pass'] += source_str[num]

            is_ok = False
            while not is_ok:
                is_ok = True
                if len([c for c in source['pass'] if c.isdigit()]) == 0:
                    is_ok = False
                    digit = source['digit'][random.randint(0, 9)]
                    source['pass'] = \
                        source['pass'].replace(source['pass'][0], digit, 1)
                if len([c for c in source['pass'] if c.islower()]) == 0:
                    is_ok = False
                    char = source['lower'][random.randint(0, 25)]
                    source['pass'] = \
                        source['pass'].replace(source['pass'][1], char, 1)
                if len([c for c in source['pass'] if c.isupper()]) == 0:
                    is_ok = False
                    char = source['upper'][random.randint(0, 25)]
                    source['pass'] = \
                        source['pass'].replace(source['pass'][2], char, 1)
                if len([c for c in source['pass'] if ispunctuation(c)]) == 0:
                    is_ok = False
                    punc = \
                        source['punc'][random.randint(0,
                                                      len(source['punc'])-1)]
                    source['pass'] = \
                        source['pass'].replace(source['pass'][3], punc, 1)
            return source['pass']

        dispatcher = {1: level_1, 2: level_2, 3: level_3, 4: level_4}
        return dispatcher[complexity](source)

    def check_password_level(self, password: str) -> int:
        """Return the password complexity level for a given password

        Complexity levels:
            Return complexity 1: If password has only lowercase chars
            Return complexity 2: Previous level condition and at least 1
                                 digit
            Return complexity 3: Previous levels condition and at least 1
                                 uppercase char
            Return complexity 4: Previous levels condition and at least 1
                                 punctuation

        Complexity level exceptions (override previous results):
            Return complexity 2: password has length >= 8 chars and
                                 only lowercase chars
            Return complexity 3: password has length >= 8 chars and
                                 only lowercase and digits

        :param password: password
        :returns: complexity level
        """
        def is_c1():
            return password.islower()

        def is_c2():
            has_lower = len([c for c in password if c.islower()]) > 0
            has_digit = len([c for c in password if c.isdigit()]) > 0
            return has_lower and has_digit

        def is_c2_exceptions():
            return len(password) >= 8 and is_c1()

        def is_c3():
            has_upper = len([c for c in password if c.isupper()]) > 0
            return is_c2() and has_upper

        def is_c3_exceptions():
            return len(password) >= 8 and is_c2()

        def ispunctuation(char):
            return char in string.punctuation

        def is_c4():
            has_punc = len([c for c in password if ispunctuation(c)]) > 0
            return is_c3() and has_punc

        complexity = 0

        if is_c1():
            complexity = 1
        if is_c2() or is_c2_exceptions():
            complexity = 2
        if is_c3() or is_c3_exceptions():
            complexity = 3
        if is_c4():
            complexity = 4
        return complexity

    def create_user(self, db_path: str) -> None:
        """Retrieve a random user from https://randomuser.me/api/
        and persist the user (full name and email) into the given SQLite db

        :param db_path: path of the SQLite db file
                        (to do: sqlite3.connect(db_path))
        :return: None
        """
        path = os.path.dirname(os.path.abspath(__file__))
        db_path = '/'.join([path, db_path])
        r = requests.get('https://randomuser.me/api/')
        info = r.json()
        name = \
            (info['results'][0]['name']['first'] +
             ' ' + info['results'][0]['name']['last']).title()
        email = info['results'][0]['email']

        conn = sqlite3.connect(db_path)
        c = conn.cursor()
        try:
            with conn:
                c.execute('''CREATE TABLE persons \
                          (Name, Email, Password, timeStamp)''')
        except sqlite3.OperationalError:
            pass
        # Insert a row of data
        c.execute("INSERT INTO persons VALUES ('{}','{}', '', {})".
                  format(name, email, time.time()))

        # Save (commit) the changes
        conn.commit()

        conn.close()


if __name__ == '__main__':
    my = generator()
    my.create_user("example.db")
